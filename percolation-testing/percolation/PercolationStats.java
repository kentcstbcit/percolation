import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;
import java.lang.*;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class PercolationStats {
  private double xarr[];
  private double mean;
  private double stddevv;
  private double confl;
  private double confh;
  public PercolationStats(int n, int trials){    // perform trials independent experiments on an n-by-n grid
    if(n<=0|| trials <= 0) 
      throw new IllegalArgumentException("can't be <= 0.");
    xarr = new double[trials];
    for(int k = 0; k < trials;k++) {
      double xs;
      Percolation sim = new Percolation(n);
      int x,y;
      while(!sim.percolates()){
        x = StdRandom.uniform(n)+1;
        y = StdRandom.uniform(n)+1;
        sim.open(x,y);
      }
      xarr[k] = (double)sim.numberOfOpenSites()/(n*n);
    }
    mean = StdStats.mean(xarr);
    stddevv = StdStats.stddev(xarr,0,trials);
    confl = mean - 1.96*stddevv/Math.sqrt(trials);
    confh = mean + 1.96*stddevv/Math.sqrt(trials);


  }
    public double mean() {                         // sample mean of percolation threshold
      return mean;
    }
    public double stddev() {                       // sample standard deviation of percolation threshold
      return stddevv;
    }
    public double confidenceLo() {                  // low  endpoint of 95% confidence interval
      return confl;
    }
    public double confidenceHi()  {                // high endpoint of 95% confidence interval
      return confh;
    }

    public static void main(String[] args) {        // test client (described below)
      int n = Integer.parseInt(args[0]);
      int t = Integer.parseInt(args[1]);

      PercolationStats one = new  PercolationStats(n,t);
      StdOut.printf("%-30s = %s","mean",Double.toString(one.mean()));
      StdOut.println();
      StdOut.printf("%-30s = %s","stddev",Double.toString(one.stddev()));
      StdOut.println();
      StdOut.printf("%-30s = [%s, %s]","95% confidence interval",Double.toString(one.confidenceLo()),Double.toString(one.confidenceHi()));
      StdOut.println();

    }

}          
