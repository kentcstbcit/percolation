import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;
import edu.princeton.cs.algs4.StdOut;

public class Percolation {
    private WeightedQuickUnionUF uf;
    private boolean[] openarr;
    int opennum;
    int  n;
    
    public Percolation(int n) {
        if(n<=0)
            throw new IllegalArgumentException("n <= 1.");
        this.n = n;
        uf = new WeightedQuickUnionUF(n*n);
        openarr = new boolean[n*n];
        opennum = 0;
        for(int x = 2; x < n+1; x=n) {
            uf.union(locate(1,1),locate(1,x));
            uf.union(locate(n,1),locate(n,x));
        }
        
    }
    
        // validate that p is a valid index
    private void validate(int row,int col) {
        if(row < 1 || row > n)
            throw new IllegalArgumentException("invalid row");
        if(col < 1 || col > n)
            throw new IllegalArgumentException("invalid col");
    }
    
    public void open(int row, int col) {
        validate(row,col);
        int indexcur = locate(row,col);
        openarr[indexcur] = true;
        if(row == 1&&isOpen(row+1,col)){
            uf.union(indexcur,indexcur+n);
        } else if(row == n && isOpen(row-1,col)){
            uf.union(indexcur,indexcur-n);
        } else if(col ==1&& isOpen(row,col+1)) {
            uf.union(indexcur,indexcur+1);
        } else if(col == n && isOpen(row, col-1)) {
            uf.union(indexcur,indexcur-1);
        } else {
            if(isOpen(row+1,col))
                uf.union(indexcur,indexcur+n);
            if(isOpen(row-1,col))
                uf.union(indexcur,indexcur-n);
            if(isOpen(row,col+1))
                uf.union(indexcur,indexcur+1);
            if(isOpen(row,col-1)) 
                uf.union(indexcur,indexcur-1);
        }
        opennum++;
    }
    
    public boolean isOpen(int row, int col) {
        return openarr[locate(row,col)];
    }
    
    public boolean isFull(int row, int col) {
        return uf.connected(locate(1,1),locate(row,col));
    }
    
    public int numberOfOpenSites(){
        return opennum;
    }
    
    public boolean percolates() {
        return uf.connected(locate(1,1),locate(n,n));
    }
    
    private int locate(int row, int col) {
        validate(row,col);
        return (row - 1) *n + (col-1);
    }
    
    public static void main(String[] args) {
        Percolation pe = new Percolation(50);
        int x,y;
        while(pe.percolates() == false) {
            x = StdRandom.uniform(400) + 1;
            y = StdRandom.uniform(400) + 1;

            StdOut.println(x);
            StdOut.println(y);
            pe.open(x,y);
        }
        
        StdOut.println(pe.numberOfOpenSites());
    }

}
