import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;
import edu.princeton.cs.algs4.StdOut;

public class Percolation {
    private WeightedQuickUnionUF uf;
    private boolean[] openarr;
    private int opennum;
    private int  n;
    private boolean[] conbtm;
    private final static int TOP=0;
    private boolean percolated;

    public Percolation(int n) {
        if(n<=0)
            throw new IllegalArgumentException("n <= 0.");
        this.n = n;
        uf = new WeightedQuickUnionUF(n*n+1);
        openarr = new boolean[n*n+1];
        conbtm = new boolean[n*n+1];
        opennum = 0;
        /*for(int x = 1; x < n+1; x++) {
            uf.union(locate(1,1),locate(1,x));
            uf.union(locate(n,1),locate(n,x));
        }*/
        
    }
    
        // validate that p is a valid index
    private void validate(int row,int col) {
        if(row < 1 || row > n)
            throw new IllegalArgumentException("invalid row");
        if(col < 1 || col > n)
            throw new IllegalArgumentException("invalid col");
    }
    
    public void open(int row, int col) {
        validate(row,col);
        int indexcur = locate(row,col);
        boolean tconbom=false;

        if(isOpen(row,col))
          return;
        if(n==1){
          uf.union(indexcur,TOP);
          openarr[indexcur] = true;
          percolated = true;
          opennum++;
          return;
        }


        openarr[indexcur] = true;
        if(row ==1)
          uf.union(indexcur,TOP);
        if(row==n)
          tconbom = true;

        if(row!=1){
          if(conbtm[uf.find(indexcur-n)])
            tconbom =true;
          if(isOpen(row-1,col))
            uf.union(indexcur,indexcur-n);
          
        }
        if(row!=n){
          if(conbtm[uf.find(indexcur+n)])
            tconbom =true;

          if(isOpen(row+1,col))
            uf.union(indexcur,indexcur+n);
                  }
        if(col!=1){
          if(conbtm[uf.find(indexcur-1)])
            tconbom =true;
          if(isOpen(row,col-1))
            uf.union(indexcur,indexcur-1);

        }

        if(col!=n){
          if(conbtm[uf.find(indexcur+1)])
            tconbom =true;
          if(isOpen(row,col+1))
            uf.union(indexcur,indexcur+1);
          
        }
        conbtm[uf.find(indexcur)] = tconbom;
        if(tconbom&&isFull(row,col))
          percolated= true;

        
        
        /*if(row == 1){
          uf.union(indexcur,TOP);
          if(isOpen(row+1,col)){
            uf.union(indexcur, indexcur+n);
          }
        } else if(row == n){
          tconbom = true;
          if((col!=n&&isFull(row,col+1))
              ||(col !=1 &&isFull(row,col-1))
              ||isFull(row-1,col))
            uf.union(indexcur,TOP);

          if(col != n && isOpen(row,col+1))
              uf.union(indexcur,indexcur+1);
          if(col != 1 && isOpen(row,col-1))
              uf.union(indexcur,indexcur-1);
          if(isOpen(row-1,col)){
            uf.union(indexcur,indexcur-n);
          }

        } else if(col ==1){
          if(isFull(row,col+1)||isFull(row-1,col)||isFull(row+1,col))
            uf.union(indexcur,TOP);


          if(isOpen(row,col+1)) {
            uf.union(indexcur,indexcur+1);
          }
          if(isOpen(row-1,col)) {
            uf.union(indexcur,indexcur-n);
          }
          if(isOpen(row+1,col)) {
            uf.union(indexcur,indexcur+n);
          }

        } else if(col == n){
          if(isFull(row,col-1)||isFull(row-1,col)||isFull(row+1,col))
            uf.union(indexcur,TOP);


          if(isOpen(row, col-1)) {
            uf.union(indexcur,indexcur-1);
          }
          if(isOpen(row-1,col)) {
            uf.union(indexcur,indexcur-n);
          }
          if(isOpen(row+1,col)) {
            uf.union(indexcur,indexcur+n);
          }
        } else {
          if(isFull(row,col+1)||isFull(row,col-1)||isFull(row-1,col)||isFull(row+1,col))
            uf.union(indexcur,TOP);


            if(isOpen(row+1,col))
                uf.union(indexcur,indexcur+n);
            if(isOpen(row-1,col))
                uf.union(indexcur,indexcur-n);
            if(isOpen(row,col+1))
                uf.union(indexcur,indexcur+1);
            if(isOpen(row,col-1)) 
                uf.union(indexcur,indexcur-1);
        }*/

        opennum++;
    }
    
    public boolean isOpen(int row, int col) {
        return openarr[locate(row,col)];
    }
    
    public boolean isFull(int row, int col) {
        return uf.connected(TOP,locate(row,col));
    }
    
    public int numberOfOpenSites(){
        return opennum;
    }
    
    public boolean percolates() {
      return percolated;
    }
    
    private int locate(int row, int col) {
        validate(row,col);
        return (row - 1) *n + col;
    }
    
    

}
